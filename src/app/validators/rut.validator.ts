import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, FormControl } from '@angular/forms';
import { rutValidate } from 'rut-helpers';

function validateRut(rutValidate: Function) {
    return (c: FormControl) => {
        let isValid = null;
        if (!rutValidate(c.value)) {
            isValid = { invalidRut: true };
        }
        return isValid;
    };
}

export function RutValidator(control: FormControl) {
    let isValid = null;
    if (!rutValidate(control.value)) {
        isValid = { invalidRut: true };
    }
    return isValid;
}

@Directive({
    selector: '[validateRut][ngModel],[validateRut][formControl]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => rutValidator), multi: true }
    ]
})
export class rutValidator {
    private validator: Function;

    constructor() {
        this.validator = validateRut(rutValidate);
    }

    public validate(c: FormControl) {
        return this.validator(c);
    }
}
