import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { ENDPOINTS, KEYS } from '../constants/endpoints.const';
import { MESSAGES } from '../constants/messages.const';
import { Observable, throwError, of, merge, concat } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class UserDetailService {
    private endpoint = '';
    private data: any = '';
    private params: any = {};
    private options: any = {};
    private headers: any = {};
    public message: string;

    constructor(private http: HttpClient) {
        this.message = 'Venta Movil Service';
    }
    private cbFailure(error: Response) {
        console.log(error);

        let auxResponse: any = {
            estado: {
            codigoEstado: _.get(error, 'error.estado.codigoEstado', '') !== '' ? _.get(error, 'error.estado.codigoEstado') : 997,
            glosaEstado: _.get(error, 'error.estado.glosaEstado', '') !== '' ? _.get(error, 'error.estado.glosaEstado') : _.get(MESSAGES, 'GENERAL.ERROR_SERVICE_FAILURE_JSON', '')
            }
        };
        try {
            auxResponse = error.json();
        } catch { return throwError(auxResponse);
        }

    }

    public getDatosCliente(dataCliente): Observable<any> {
        console.log(dataCliente);
        this.data = {};
        this.params = {};
        this.endpoint = _.get(ENDPOINTS, 'client.GET_USER_DETAIL', '');
        this.endpoint = this.endpoint.replace('{rut}', _.get(dataCliente, 'rut', ''));
        return this.http.get(this.endpoint, this.params)
            .pipe(catchError(this.cbFailure));
    }


}
