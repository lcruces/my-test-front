import { Injectable } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Observable, throwError, of, merge, concat } from 'rxjs';
import { map, catchError  } from 'rxjs/operators';
import { UserDetailService } from './consultarut.service';
import _ from 'lodash';

@Injectable()
export class ConsultarutProvider{
  public estado: string = '';
  public dosis = {};
  constructor(
    private consultaRutSrv: UserDetailService,
  ) { }



    public consultaData(datosCliente: any) {

        const cbSuccessOk = (response) => {

          return response;
        };
        const cbFailure = (error) => {
            return throwError(error);
        };



        return this.consultaRutSrv.getDatosCliente(datosCliente)
            .pipe(map(cbSuccessOk))
            .pipe(catchError(cbFailure));

    }

}