import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { RutValidator } from '../../validators/rut.validator';
import { ConsultarutProvider } from '../../providers/consultarut.provider';
import { Router, RoutesRecognized } from '@angular/router';
import _ from 'lodash';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public formRut: any;
  public dosis: {};
  public dosis2: {};
  public estado: string = '';
  public fecha: any;

  constructor(
    private fb: FormBuilder,
    private consultaRutPrvdr: ConsultarutProvider,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    this.router.events.subscribe(event => {
      if (event instanceof RoutesRecognized) {
        this.ngOnInit();
      }
    });
  }

  ngOnInit(): void {

    this.formRut = this.fb.group({
      rut: new FormControl('', [RutValidator, Validators.required]),
      serie: new FormControl('', [Validators.required])
    });
  }


  public continuar(form) {

    console.log(form.value.rut);

    const datosCliente = {
      rut: form.value.rut,
    };
    const cbSuccessCliente = (response) => {

      this.estado = _.get(response, 'estado.codigoEstado', '');
      this.dosis = _.get(response, 'result', '');
    };

    const cbFailure = async error => {
      this.snackBar.open(error.estado.glosaEstado, '', {
        duration: 3000,
        verticalPosition: 'top'
      });
    };

    this.consultaRutPrvdr.consultaData(datosCliente).subscribe(cbSuccessCliente, cbFailure);
  }

}
