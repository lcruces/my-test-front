export const KEYS = {
    APIKEYS: {
        PROD: 'zaV0FYE5W39TUtAhLrC1RljHkdoTYeB9',
        TEST: 'oDgl4Ju90OdGJogG2bCZBRWmCGQyGwbQ',
        DEV: 'oDgl4Ju90OdGJogG2bCZBRWmCGQyGwbQ',
        APIKEY_PARAM_CAT: 'ln40ALVCOHIWY02s3NE7hNOojAERIqKR',
        APIKEY_BILLING_EVENTS: 'rMxEn2tY26bAfYKriGYvUJ3Mxols2uuA',
        APIKEY_VIGENCIA_RUT: '37d33e9d-04a4-41a0-bd99-d18ae11cfc8c',
        APIKEY_PORTA: 'rMxEn2tY26bAfYKriGYvUJ3Mxols2uuA',
        APIKEY_SMS_MIGRA: 'w8kfm8dYR59V3Ithu6mw3CTUhD9bGhzv',
        APIKEY_AXWAY: '3d0885cc-8202-45c8-9897-8b2efcdf3ba0',
        APIKEY_AXWAY_PROD: '37d33e9d-04a4-41a0-bd99-d18ae11cfc8c',
        API_KEY_FIRMA_DOCUMENTO: '5c96e35a9c6ab836e9019168265e06c45f25c7d9'
    },
    BASIC: 'ZXZlcmlzOmV2ZXJpc2FwcHNAdGVsZWZvbmljYS5jb20=',
    BASIC_BIOMETRIA: 'ZGlhZ25vc2lzOmRpYWdub3Npc0B0ZWxlZm9uaWNhLmNvbQ==',
    CLIENT_SECRET: '773123e0-3127-4cbc-ba14-c5d146fa3c06', // '0b1b9JxQKh9eE40t',
    CLIENT_ID: '2c66b791-37fb-472d-b5b9-93c1bd6e9ddd', // 'zaV0FYE5W39TUtAhLrC1RljHkdoTYeB9',
    PARAM_CAT_ACCESS_TOKEN: 'bW9uaXRvcmVvOm1vbml0b3Jlb0B0ZWxlZm9uaWNhLmNvbQ==',
    BASIC_PROD_AXWAY: 'MzdkMzNlOWQtMDRhNC00MWEwLWJkOTktZDE4YWUxMWNmYzhjOmFkYmE0OGI2LTYwMzAtNDU4OS04MzU2LTgxYzRkYzhhOTU2Zg==',
};

export const ENDPOINTS = {
    client: {
        GET_USER_DETAIL: 'http://localhost:8010/userDetail/{rut}',
    }
};
